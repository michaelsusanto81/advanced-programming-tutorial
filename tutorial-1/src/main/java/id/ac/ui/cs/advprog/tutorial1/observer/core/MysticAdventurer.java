package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                //ToDo: Complete Me
                this.guild = guild;
                this.guild.add(this);
        }

        @Override
        public void update() {
                String questType = guild.getQuestType();
                if(questType.equals("D") || questType.equals("E")) {
                        Quest quest = guild.getQuest();
                        this.getQuests().add(quest);
                }
        }

        //ToDo: Complete Me
}
